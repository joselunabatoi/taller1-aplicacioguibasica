package dam.joseluna.u3t1initialapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private int count;
    private TextView tvDisplay;
    private Button buttonIcrease, buttonDecrease,buttonReset,buttonIcrease2, buttonDecrease2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        //TODO: añadimos los dos botones nuevos
        tvDisplay = findViewById(R.id.tvDisplay);
        buttonIcrease = findViewById(R.id.buttonIncrease);
        buttonDecrease = findViewById(R.id.buttonDecrease);
        buttonReset = findViewById(R.id.buttonReset);
        buttonIcrease2 = findViewById(R.id.buttonIncrease2);
        buttonDecrease2 = findViewById(R.id.buttonDecrease2);

        buttonIcrease.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
        buttonDecrease2.setOnClickListener(this);
        buttonIcrease2.setOnClickListener(this);
    }

    //TODO: guardar los datos al girar la pantalla
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("CONT", count);
    }
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        count = savedInstanceState.getInt("CONT");
        tvDisplay.setText(String.valueOf("Numbre of elements : "+count));
    }

    @Override
    public void onClick(View view) {
        //TODO: creamos que en caso de que se pulse ese boton se le sume o reste 2
        switch (view.getId()){
            case R.id.buttonIncrease:
                count++;
                break;
            case R.id.buttonDecrease:
                count--;
                break;
            case R.id.buttonReset:
                count = 0;
                break;
            case R.id.buttonIncrease2:
                count += 2;
                break;
            case R.id.buttonDecrease2:
                count -= 2;
                break;
        }

        tvDisplay.setText(getString(R.string.numbre_of_elemnts) + " : " + count);

    }
}